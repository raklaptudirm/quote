type QuoteBlockType = "emphasis" | "normal"

const EMPHASIS: QuoteBlockType = "emphasis"
const NORMAL: QuoteBlockType = "normal"

interface QuoteFormat {
  type: QuoteBlockType
  data: string
}

interface Quote {
  data: QuoteFormat[]
  favourite: boolean
}

interface QuoteString {
  data: string
  favourite: boolean
}

interface Database {
  quotes: QuoteString[]
  people: string[]
}